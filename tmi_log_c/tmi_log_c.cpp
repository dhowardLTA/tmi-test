#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#include <conio.h>
#include <stdio.h>
int main()
{
    WSADATA wsa;
    SOCKET lcl_sock;
    char buf[2048];
    int status;
    sockaddr_in lcl_addr;

    (void)WSAStartup(MAKEWORD(2, 2), &wsa);

    lcl_sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (lcl_sock == INVALID_SOCKET) {
        printf("invalid socket %d\n", GetLastError());
        return 1;
    }

    memset(&lcl_addr, 0, sizeof(lcl_addr));
    lcl_addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
    lcl_addr.sin_port = htons(14300);
    lcl_addr.sin_family = AF_INET;
    status = bind(lcl_sock, (sockaddr *)&lcl_addr, sizeof(lcl_addr));
    if (status == SOCKET_ERROR) {
        printf("bind error %d\n", GetLastError());
        return 3;
    }

    while (!_kbhit()) {
        memset(buf, 0, sizeof(buf));
        status = recv(lcl_sock, buf, sizeof(buf), 0);
        if (status == SOCKET_ERROR) {
            printf("recv error %d\n", GetLastError());
            return 2;
        }
        printf("%s", buf);
    }
}
